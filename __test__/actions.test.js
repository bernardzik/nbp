import { store, loadedCurrencies, deleteCurrency } from '../src/actions/index';
import { doApiGet } from '../src/shared/api';

describe('loadedCurrencies', () => {
  it('should create an action to pass currencies', () => {

    let currencies = [{code:"USD", currency:"dolar amerykański"}];
    let expectedAction = {
      type: 'LOADED_CURRENCIES',
      currencies
    };

    expect(loadedCurrencies(currencies)).toEqual(expectedAction);

  })
})

describe('loadCurrencies', () => {
  it('should load currency data from server', (done) => {

    return doApiGet('exchangerates/rates/a/usd/')
      .then((data) => {
        expect(data.data.code).toEqual('USD');
        done();
      });

  })
})

describe('deleteCurrency', () => {
  it('should delete currency and create an action to pass currencies', () => {

    let currencies = [{code:"USD", currency:"dolar amerykański"}, {code:"CHF", currency:"funt szwajcarski"}];
    let newCurrencies = currencies.filter(function(el) {
        return el.code !== 'USD';
    });
    let expectedAction = {
      type: 'LOADED_CURRENCIES',
      currencies: [{code:"CHF", currency:"funt szwajcarski"}]
    };

    expect(loadedCurrencies(newCurrencies)).toEqual(expectedAction);

  })
})
