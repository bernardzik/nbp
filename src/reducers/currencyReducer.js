export const CurrencyReducer = (state = {}, action) => {
   switch (action.type) {
     case 'LOADED_CURRENCIES':
       return {
         currencies: action.currencies
       };
     default:
       return state;
   }
}
