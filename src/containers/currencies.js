import React from 'react';
import { connect } from 'react-redux'
import { loadCurrencies, deleteCurrency } from '../actions/index';

let Currencies = ({ loadCurrencies, deleteCurrency, currencies }) => {

  let currencyCode;
  console.log('currencies view', currencies);

  function clearCurrencyCode() {
    currencyCode.value="";
  }

  return (
    <div className='container'>
      <div className='panel panel-default'>
        <div className='panel-heading'>Dodaj Walutę</div>
        <div className='panel-body'>
          <div className='input-group'>
            <input type='text' ref={(ref) => {currencyCode = ref}} placeholder='Wpisz kod waluty np. usd, chf, gbp, eur' className='form-control'
              onKeyDown={(event) => {if(event.keyCode == 13) document.getElementById('btnSearch').click()}} />
            <span className='input-group-btn'>
              <button id="btnSearch" type='button' className='btn btn-success' onClick={() => {loadCurrencies(currencyCode.value); clearCurrencyCode();}}>Dodaj</button>
            </span>
          </div>
        </div>
      </div>
      <div className='panel panel-default'>
        <div className='panel-heading'>Lista Walut (kurs według PLN)</div>
        <div className='panel-body'>
          <table className='table table-striped'>
            <thead>
            <tr>
              <th>Code</th>
              <th>Name</th>
              <th>Table</th>
              <th>Date</th>
              <th>Price</th>
              <th></th>
            </tr>
            </thead>
            <tbody>
              {currencies && currencies.map((currency) =>
                <tr key={currency.code}>
                  <td>{currency.code}</td>
                  <td>{currency.currency}</td>
                  <td>{currency.table}</td>
                  <td>{currency.rates[0].effectiveDate}</td>
                  <td>{currency.rates[0].mid}</td>
                  <td><button className="btn btn-danger" onClick={() => deleteCurrency(currency.code)}>Usuń</button></td>
                </tr>
              )}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  )
}

const mapStateToProps = (state) => {
  return {
    currencies: state.CurrencyReducer.currencies
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    loadCurrencies: (currencyCode) => {
      dispatch(loadCurrencies(currencyCode))
    },
    deleteCurrency: (currencyCode) => {
      dispatch(deleteCurrency(currencyCode))
    }
  }
}

Currencies = connect(mapStateToProps, mapDispatchToProps)(Currencies);

export default Currencies;
