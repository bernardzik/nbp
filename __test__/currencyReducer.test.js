import { CurrencyReducer } from '../src/reducers/currencyReducer';

describe('currencyReducer', () => {
  it('should handle LOADED_CURRENCIES', () => {

    let currencies = [{code:"USD", currency:"dolar amerykański"}];

    expect(
      CurrencyReducer([], {
        type: 'LOADED_CURRENCIES',
        currencies
      })
    ).toEqual(
      {
        currencies: [{code:"USD", currency:"dolar amerykański"}]
      }
    );

  })
})
