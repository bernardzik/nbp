import React from 'react';
import styles from './app.css';

let App = ({ children }) => {
  return (
    <div>
     <nav className='navbar navbar-default'>
       <div className='container'>
         <div className='navbar-header'>
           <button type='button' className='navbar-toggle collapsed' data-toggle='collapse' data-target='#bs-example-navbar-collapse-1' aria-expanded='false'>
             <span className='sr-only'>Toggle navigation</span>
             <span className='icon-bar'></span>
             <span className='icon-bar'></span>
             <span className='icon-bar'></span>
           </button>
           <a className={styles.brand + ' navbar-brand'} href='#'><img src={require('../static/nbp-logo.png')} height='30' width='142' /></a>
         </div>
         <div className='collapse navbar-collapse' id='bs-example-navbar-collapse-1'>
           <ul className={styles.link + ' nav navbar-nav'}>
             <li><a href="#">Waluty</a></li>
           </ul>
         </div>
       </div>
     </nav>
     {children}
   </div>
  )
}

export default App;
