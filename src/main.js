import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { Router, Route, IndexRoute, hashHistory } from 'react-router';
import { store } from './actions/index';

import App from './containers/app';
import Currencies from './containers/currencies';

ReactDOM.render(
  <Provider store={store}>
    <Router history={hashHistory}>
      <Route path="/" component={App}>
        <IndexRoute component={Currencies}/>
      </Route>
    </Router>
  </Provider>,
  document.getElementById('mount')
)
