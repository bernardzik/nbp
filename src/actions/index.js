import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import rootReducer from '../reducers/index';
import { doApiGet } from '../shared/api';

import { hashHistory } from 'react-router';
import { routerMiddleware, push } from 'react-router-redux';

const middleware = routerMiddleware(hashHistory);

export const store = createStore(
  rootReducer,
  applyMiddleware(thunk, middleware)
);

export function loadedCurrencies(currencies){
  return {
    type: 'LOADED_CURRENCIES',
    currencies
  }
}

//thunks

export function loadCurrencies(currencyCode) {
  return (dispatch) => {
    console.log('[loadCurrencies]');
    doApiGet('exchangerates/rates/a/'+currencyCode+'/').then((data)=>{
      console.log('loadedCurrency', data.data);
      let currencies = [];
      if (store.getState().CurrencyReducer.currencies) {
        Object.assign(currencies, store.getState().CurrencyReducer.currencies);
        currencies.push(data.data);
      } else {
        currencies.push(data.data);
      }
      dispatch(loadedCurrencies(currencies));
    }).catch((error) => {
      console.log('loadCurrencies error', error);
      alert('Nie znaleziono danej waluty.');
    })
  }
}

export function deleteCurrency(currencyCode) {
  return (dispatch) => {
    console.log('[deleteCurrency]');
    let currencies, newCurrencies;
    currencies = store.getState().CurrencyReducer.currencies;
    newCurrencies = currencies.filter(function(el) {
        return el.code !== currencyCode;
    });
    dispatch(loadedCurrencies(newCurrencies));
  }
}
